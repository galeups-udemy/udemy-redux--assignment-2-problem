import React, { Component } from 'react';
import { connect } from 'react-redux';

import Person from '../components/Person/Person';
import AddPerson from '../components/AddPerson/AddPerson';

class Persons extends Component {

    personAddedHandler = () => {
        const action = {
            type: 'ADD'
        };

        this.props.addPerson(action);
    }

    personDeletedHandler = (personId) => {
        const action = {
            type: 'REMOVE',
            payload: personId
        };

        this.props.removePerson(action);
    }

    render() {
        const { persons } = this.props;

        return (
            <div>
                <AddPerson personAdded={this.personAddedHandler} />
                { persons.map(person => (
                    <Person
                        key={person.id}
                        name={person.name}
                        age={person.age}
                        clicked={() => this.personDeletedHandler(person.id)}/>
                ))}
            </div>
        );
    }
}

const mapStateToPops = state => {

    return {
        persons: state.persons
    };
};

const mapDispatchToProps = dispatch => {
    return {
        addPerson: (action) => dispatch(action),
        removePerson: (action) => dispatch(action)
    };
};

export default connect(mapStateToPops, mapDispatchToProps)(Persons);