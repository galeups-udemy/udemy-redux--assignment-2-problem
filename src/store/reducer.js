const initialState = {
    persons: []
};

const reducer = (state = initialState, action) => {

    if (action.type === 'ADD') {

        const newPerson = {
            id: Math.random().toFixed(5) * 100000,
            name: 'Max',
            age: Math.floor(Math.random() * 40)
        };

        return {
            ...state,
            persons: state.persons.concat(newPerson)
        };
    }

    if (action.type === 'REMOVE') {
        console.log(action);
        return {
            ...state,
            persons: state.persons.filter(person => person.id !== action.payload)
        };
    }
    return state;
};

export default reducer;